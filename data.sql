-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Okt 2020 pada 05.29
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_table`
--

CREATE TABLE `data_table` (
  `id_char` int(11) NOT NULL,
  `nama` varchar(99) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `senjata` varchar(50) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_table`
--

INSERT INTO `data_table` (`id_char`, `nama`, `role`, `senjata`, `gender`) VALUES
(43, 'vace', 'Assasin', 'Knife', 'Male'),
(46, 'enix', 'Mage', 'Knife', 'Male'),
(47, 'jans', 'Mage', 'Staff', 'Male'),
(48, 'vasana', 'Support', 'Staff', 'Female');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `data_table`
--
ALTER TABLE `data_table`
  ADD PRIMARY KEY (`id_char`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `data_table`
--
ALTER TABLE `data_table`
  MODIFY `id_char` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
include "inc/connection.php";
?>

<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <!--CSS from Asset-->
  <link rel="stylesheet" href="asset/css/Style.css">

  <title>Dashboard</title>
</head>
<body>
  
  <!-- background image -->
   <div class="fullscreen-bg">
    <img src="asset/image/bg_3.png" id="BackgroundImageEdit" alt="not Found">
  </div>

  
  
  <!-- Navbar Header -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="navbar_header"  >
    <a class="navbar-brand Font1 text-light" href="https://github.com/SGWN"><img style="width: 40px; border-radius: 50%;" id="profil" src="asset/image/SGWN.jpg" alt="aa">
</a> <h2 class="navbar-brand Font1 text-light" style="margin-left: 400px;">Dashboard</h2>   
</nav>

<!-- Body Card -->
<div class="container">
  <div class="row"> 
    <!--empty text for end of line -->
    <div class="col-3">
      <h3 class="Font1 text-danger" style="margin-top: 60px ;" id="null_text"></h3>
    </div>
  </div>
</div>

<div class="container-fluid ">
  <div class="row">
   <div class="col-2 bg-dark z-index">
     <h3 class="Font1 mt-4" style="font-size: 20px;"> <a href="https://github.com/SGWN" style="text-decoration: none; color:white">About Me</a></h3>
   </div>    
   <div class="col-10">

     <!-- header tittle -->
     
     <div class="container">
       <div class="row">
         <h3 class="Font1 text-danger mt-3 z-index" id="body_title">Your Character Cards Cast </h3>
         <div class="col-3">
           </div>
          </div>
        </div>
        
        <!-- body cards -->
        <div class="container">
          <div class="row mb-5">
            <?php foreach($char as $row) : ?>
              <div class="card m-3 mr-3 bg-dark z-index" id="card_body">
                <div class="card-body Font1">
                  
                  
                  <!-- Card -->
                  <p class="card-title text-light ml-2" id="card_header" ><?=$row['nama'];?></p>
                  <ul style="list-style: none;">
                    <li>
                      <img  id="CardImg" class="mx-auto" src="asset/image/bg_card2.png" alt="not found" >
                      <p class=" text-light">Role : <?=$row['role'];?></p>
                    </li>
                    <li>
                      <p class="text-light">Weapon: <?=$row['senjata'];?></p>
                    </li>
                    <li>
                      <p class="text-light">Gender : <?=$row['gender'];?></p>
                  </li>
                </ul>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
    </div>
    
    
        

<!-- font awesome -->
<script src="https://kit.fontawesome.com/6dcdfb065e.js" crossorigin="anonymous"></script>
<!-- Option 1: jQuery and Bootstrap Bundle  -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>


</body>
</html>
<!-- this web site is create by akbar kurnia -->